<?php
    require_once('code/4139.php');
    $user = new userdata(FALSE);
    $errorcode;
    if(isset($_POST['error']))
    {
        $errorcode = $_POST['error'];
    }
    if($errorcode == 'noerr'){
        header("Location: http://4139.kylelem.com/index.php");
    }
    
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Register for 4139</title>
        <link rel="stylesheet" type="text/css" href="main.css"/>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        
    </head>
    <body>
        <div id="main">
            <h1>Register for Team 4139</h1>
            <p class="italic">Complete the form below to reigster. Please note your account may be deleted 
            due to inactivity (both on this site and in the team).</p>
            
            <form id="reg" method="post" onkeyup="validate()" onsubmit="return validate()" onmouseup="validate()" action="performregister.php">
                <table>
                    <tr>
                        <td>
                            <p>Full Name:</p>
                        </td>
                        <td>
                            <input type="text" name="name"></input>
                            <i id="name">
                                <?php
                                    if($errorcode == "name") 
                                        echo $_POST["name"];
                                ?>
                            </i>
                            
                            <note>First and last name separated by a space.</note>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p>Position:</p>
                        </td>
                        <td>
                            <select form="reg" name ="position">
                                <option value="member">Member</option>
                                <option value="officer">Officer</option>
                                <option value="lead">Department Lead</option>
                                <option value="treasurer">Treasurer</option>
                                <option value="secretary">Secretary</option>
                                <option value="vicepres">Vice President</option>
                                <option value="pres">President</option>
                                <option value="mentor">Mentor</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p>Email:</p>
                        </td>
                        <td>
                            <input type="text" name="email" />
                            <i id="email">
                                <?php 
                                    if($errorcode == "email") 
                                        echo $_POST["email"];
                                ?>
                            </i>
                            <note>Must be valid to complete registration.</note>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p>Password:</p>
                        </td>
                        <td>
                            <input type="password" name="password" />
                            <i id="password">
                                <?php 
                                    if($errorcode == "password") 
                                        echo $_POST["password"];
                                ?>
                            </i>
                            <note>At least 8 characters.  Must contain one capital letter and one number.  No spaces.</note>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p>Confirm Password:</p>
                        </td>
                        <td>
                            <input type="password" name="conf" />
                            <i id="conf"></i>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p>Cell Phone Number:</p>
                        </td>
                        <td>
                            <input type="tel" name="phone">
                            <select form="reg" name="carrier">
                                <option value="default">Choose your cell carrier</option>
                                <option value="@txt.att.net">AT&T</option>
                                <option value="@vtext.com">Verizon</option>
                                <option value="@tmomail.net">T-Mobile</option>
                                <option value="@messaging.sprintpcs.com">Sprint PCS</option>
                                <option value="@vmobl.com">Virgin Mobile</option>
                                <option value="@myboostmobile.com">Boost Mobile</option>
                                <option value="@mymetropcs.com">Metro PCS</option>
                                <option value="@pcs.rogers.com">Rogers AT&T Wireless</option>
                            </select>
                            <i id="phone">
                                <?php 
                                    if($errorcode == "phone") 
                                        echo $_POST["phone"];
                                ?>
                            </i>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p>Choose groups:</p>
                        </td>
                        <td>
                            <?php
                                $groups = $user->get_groups();
                                foreach($groups as $group){
                                    echo "<input type='checkbox' name='groups[]' value='".$group['code']."'>".$group['name']."</input>";
                                }
                            ?>
                        </td>
                    </tr>
                </table>
                <input type="submit" value="Register">
            </form>

            
        </div>
        <script type="text/javascript">



            function validateEmail(email)
            {
                var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
                return re.test(email);
            }

            function validPassword(password)
            {
                var re = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}$/;
                return re.test(password);
            }

            function validPhone(phone)
            {
                var re = /^\d{10}$/;
                return re.test(phone);
            }

            function validate()
            {
                var noerr = true;
                var name = document.forms['reg']['name'].value.trim();

                if (name.indexOf(' ') == -1 && name != "")
                {
                    document.getElementById('name').innerHTML = "Invalid Name.";
                    noerr = false;
                }
                else
                {
                    document.getElementById('name').innerHTML = "";
                }

                var email = document.forms['reg']['email'].value.trim();
                if (!validateEmail(email) && email != "")
                {
                    document.getElementById('email').innerHTML = "Invalid email address.";
                    noerr = false;
                }
                else
                {
                    document.getElementById('email').innerHTML = "";
                }

                var password = document.forms['reg']['password'].value.trim();
                var conf = document.forms['reg']['conf'].value.trim();
                if (!validPassword(password) && password != "")
                {
                    document.getElementById('password').innerHTML = "Invalid password.";
                    noerr = false;
                }
                else
                {
                    document.getElementById('password').innerHTML = "";
                }
                if ((password != conf) && conf != "")
                {
                    document.getElementById('conf').innerHTML = "Passwords must match.";
                    noerr = false;
                }
                else
                {
                    document.getElementById('conf').innerHTML = "";
                }


                var phone = document.forms['reg']['phone'].value.trim();
                var carrier = document.forms['reg']['carrier'].value;
                if (!validPhone(phone) && phone != "")
                {
                    document.getElementById('phone').innerHTML = "Not a valid phone number.";
                    noerr = false;
                }
                else
                {
                    document.getElementById('phone').innerHTML = "";
                }
                if (carrier == "default" && phone != "")
                {
                    document.getElementById('phone').innerHTML += " Select your carrier.";
                    noerr = false;
                }
                else
                {
                    if (!validPhone(phone) && phone != "")
                    {
                        document.getElementById('phone').innerHTML = "Not a valid phone number.";
                        noerr = false;
                    }
                    else
                    {
                        document.getElementById('phone').innerHTML = "";
                    }
                }

                if (name == "" || email == "" || password == "" || conf == "" || phone == "" || carrier == "default")
                    noerr = false;

                return noerr;
            }
        </script>
    </body>
</html>
<?php
    echo "error: ".$errorcode;
?>