<?php
require_once('../code/4139.php');
$user = new userdata(TRUE);
    
if(isset($_GET['query']))
{
    $query = $_GET['query'];
    $vals = $user->search($query);
    $groups = $user->get_groups();
}

if($vals){
    foreach($vals as $val){
        echo "@".$val['name'].", ";
        echo $val['position'].", ";
        echo $val['email'].", ";
        echo $val['phone']."<br>";
    }
    foreach($groups as $group){
        echo ".".$group['name'].", ";
        echo $group['leader'].", ";
        echo $group['officer'].", ";
        echo $group['code']."<br>";
    }
}
?>
