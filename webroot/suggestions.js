
/**
 * Provides suggestions for state names (USA).
 * @class
 * @scope public
 */
function UserSuggestions() {
    var xmlhttp = new XMLHttpRequest();
    var data;
    xmlhttp.onreadystatechange = function ()
    {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
        {
            var lines = xmlhttp.responseText.split("<br>");
            data = new Array(lines.length);
            var i = 0;
            for (x in lines)
            {
                data[x] = lines[x].split(",")[0];
            }
            
        }
    }
    
    xmlhttp.open("GET", "/ajax/query.php?query=", false);
    xmlhttp.send();
    this.users = data; 
    console.log(this.users);
}
UserSuggestions.prototype.contains = function (text)
{
    text = text.toLowerCase();
    //search for matching states
    for (var i = 0; i < this.users.length; i++)
    {
        if (this.users[i].toLowerCase().indexOf(text) == 1)
        {
            return this.users[i];
        }
    }
    return false;
}
/**
 * Request suggestions for the given autosuggest control. 
 * @scope protected
 * @param oAutoSuggestControl The autosuggest control to provide suggestions for.
 */
UserSuggestions.prototype.requestSuggestions = function (oAutoSuggestControl /*:AutoSuggestControl*/,
                                                          bTypeAhead /*:boolean*/) {
    var aSuggestions = [];
    var sTextboxValue = oAutoSuggestControl.textbox.value.toLowerCase();
    
    if (sTextboxValue.length > 0){
    
        //search for matching states
        for (var i=0; i < this.users.length; i++) { 
            if (this.users[i].toLowerCase().indexOf(sTextboxValue) == 1) {
                aSuggestions.push(this.users[i]);
            } 
        }
    }

    //provide suggestions to the control
    oAutoSuggestControl.autosuggest(aSuggestions, bTypeAhead);
};