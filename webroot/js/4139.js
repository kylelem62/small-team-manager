

var SESSION = SESSION || {
    logged: 1,
    time: 0,

    //User Information
    name: 0,
    email: 0,
    phone: 0,
    carrier: 0,
    position: 0,

    init: function () {
        this.reload();
        
        window.setInterval(this.reload, 1000);
    },
    authenticate: function () {
        get("auth.php", check);
        function check(text) {
            if (text == "false" && SESSION.logged) {
                SESSION.logged = false;
                PAGE.overlaylogin();
            }
            else
                this.logged = true;
        }
    },
    reload: function () {
        SESSION.authenticate();
        
        get("cookieexp.php", expire);
        function expire(time) {
            get("user.php", finished);
            SESSION.time = time;
            function finished(text) {
                if (text == "")
                    return;
                var items = text.split(";");
                SESSION.name = items[0];
                SESSION.email = items[1];
                SESSION.phone = items[2];
                SESSION.carrier = items[3];
                SESSION.position = items[4];
                PAGE.update();
            }
        }
        
    },
    login: function (email, password, onlogged) {
        post("login.php", fin, "email=" + email + "&password=" + password);
        function fin(text) {
            SESSION.reload();
            onlogged(text);

        }
    },
    logout: function () {
        get("logout.php", logfinished);
        function logfinished() {
            window.location = "/";
        }
    },
    change: function (name,email,password,phone,carrier,groups){
        post("change.php",finished,"name="+name+"&email="+email+"&password="+password+"&phone="+phone+"&carrier="+carrier+"&groups="+groups);
        function finished(text){
            
        }
    }
};

var PAGE = PAGE || {
    sessionfooter: 0,
    overlay: 0,
	 displayed: 0,
    names: 0,
    phones: 0,
    emails: 0,
    carriers: 0,
    positions: 0,

    init: function () {
        this.sessionfooter = document.createElement("p");
        this.sessionfooter.innerHTML = "Loading...";
        document.body.appendChild(this.sessionfooter);

        this.overlay = document.createElement("div");
        this.overlay.setAttribute("id", "overlay");
        this.overlay.innerHTML = "";
        document.body.appendChild(this.overlay);

        names = document.getElementsByClassName("name");
        phones = document.getElementsByClassName("phone");
        emails = document.getElementsByClassName("email");
        carriers = document.getElementsByClassName("carrier");
        positions = document.getElementsByClassName("position");
    },
    overlaylogin: function () {
        this.hideoverlay();
        console.log("overlay called");
        $(PAGE.overlay).load("overlay.html", function () {
            console.log("log");
        });

    },
    overlaystatus: function (title, information) {
        this.hideoverlay();
        $(PAGE.overlay).load("/status.html", finished);
        function finished() {
            status(title, information);
        }
    },
    hideoverlay: function () {
        if (PAGE.overlay && PAGE.overlay.parentNode)
            PAGE.overlay.innerHTML = "";
    },
    update: function () {
        var fmat = (Math.floor(SESSION.time / 3600)) + ":"
            + (Math.floor(SESSION.time / 60) % 60) + ":"
            + SESSION.time % 60;
        PAGE.sessionfooter.innerHTML = "Your session expires in: " + fmat + " (hh:mm:ss). " +
            "<input type='button' value='Log Out' onclick='SESSION.logout()'>";
        SESSION.time--;
        if(PAGE.displayed == 0){
        	
   	     for (var i = 0; i < names.length; i++) {
      	      if (names[i].innerHTML != SESSION.name)
         	       names[i].innerHTML = SESSION.name;
        		}
   	     for (var i = 0; i < phones.length; i++) {
      	      if (phones[i].innerHTML != SESSION.phone)
         	       phones[i].innerHTML = SESSION.phone;
	        }
   	     for (var i = 0; i < emails.length; i++) {
      	      if (emails[i].innerHTML != SESSION.email)
         	       emails[i].innerHTML = SESSION.email;
	        }
   	     for (var i = 0; i < carriers.length; i++) {
      	      if (carriers[i].innerHTML != SESSION.carrier)
         	       carriers[i].innerHTML = SESSION.carrier;
 	       }
   	     for (var i = 0; i < positions.length; i++) {
      	      if (positions[i].innerHTML != SESSION.position)
         	       positions[i].innerHTML = SESSION.position;
        	}
        	PAGE.displayed = 1;
     	}
    }
};


function init(){
    PAGE.init();
    SESSION.init();
    console.log("init");
}

$(document).ready(init);

/*
    Page must be an absolute path from the root directory of the webpage.
    Callback must have a text variable included, as it is set to the response
    text of the AJAX call.    
   */
function get(page, callback) {
    var xmlhttp;
    if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    }
    else {// code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    //Call callback with parameters
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === 4 && xmlhttp.status === 200)
            callback(xmlhttp.responseText);
    };

    xmlhttp.open("GET", "/ajax/"+page, true);
    xmlhttp.send();
}

function post(page, callback, data){
    var xmlhttp;
    if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    }
    else {// code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    //Call callback with parameters
    xmlhttp.onreadystatechange = function () { callback(xmlhttp.responseText) };

    xmlhttp.open("POST", "/ajax/"+page, true);
    xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    xmlhttp.send(data);
}

function search(str,callback){
    SESSION.authenticate();
    get("query.php?query="+str,callback);
}
