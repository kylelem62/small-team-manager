<?php
    require_once('code/4139.php');
    $user = new userdata(FALSE);

    $name = $_POST['name'];
    $email = $_POST['email'];
    $position = $_POST['position'];
    $password = $_POST['password'];
    $phone = $_POST['phone'];
    $carrier = $_POST['carrier'];
    $groups = $_POST['groups'];
    
    if($user->register($name,$position,$email,$password,$phone,$carrier,$groups))
    {
        $user->login($email.';'.$password);
        //header("Location: http://4139.kylelem.com/");
    }
    
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Submission Successfull...</title>
        <link rel="stylesheet" type="text/css" href="main.css">
    </head>
    <body>
        <h1>
	    Your registration has been accepted <?php echo $name;?>.
	</h1>
	<a href="/register.php">Submit another registration</a>
</html>
