<?php
    require_once('code/4139.php');
    $user = new userdata();

    $vals = $user->search("");
    $emails;
    foreach($vals as $val){
        $emails .= "'".$val['email']."',";
    }
?>

<!DOCTYPE html>
<html lang="en-us">
    <head>
        <meta charset="utf-8" />
        <title>Send E-Mail</title>
        <link rel="stylesheet" type="text/css" href="css/main.css"/>
        <script type="text/javascript" src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
        <script type="text/javascript" src="js/4139.js"></script>
        <script src="//tinymce.cachefly.net/4.1/tinymce.min.js"></script>
        <script>
            tinymce.init({
                selector: "textarea",
                theme: "modern",
                plugins: [
                    "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                    "searchreplace wordcount visualblocks visualchars code fullscreen",
                    "insertdatetime media nonbreaking save table contextmenu directionality",
                    "emoticons template paste textcolor colorpicker textpattern"
                ],
                toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
                toolbar2: "print preview media | forecolor backcolor emoticons",
                image_advtab: true,
                templates: [
                    {title: 'Test template 1', content: 'Test 1'},
                    {title: 'Test template 2', content: 'Test 2'}
                ]
            });
        </script>
        <script type="text/javascript" src="autosuggest.js"></script>
        <script type="text/javascript" src="suggestions.js"></script>

        <script>
            extraemaillist = {};
            extrasCount = 0;
        </script>
    </head>
    <body>
        <div id="main">
            <h1>Send an E-Mail to Team Members</h1>
            <p class="italic"></p>

            <form action="sendmail.php" id="sendemail" method="POST" onsubmit="return isreadytosubmit()">
                <table>
                    <tr>
                        <td>
                            <p>Your Name:</p>
                        </td>
                        <td>
                            <p><var class="name"></var></p>
                            <input type="hidden" name="name" value="<?php echo $user->name?>">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p>Send to:</p>
                        </td>
                        <td>
                            
                            <input type="text" onkeypress="if(event.keyCode==13)return false;" id="to">
                            <div id="email_groups"></div>
                            <br style="clear: left"/>
                            <script>
                                function removeGroup(id)
                                {
                                    var elems = document.getElementsByClassName("list");
                                    for (x in elems)
                                    {
                                        if (elems[x].id == id)
                                        {
                                            elems[x].parentNode.removeChild(elems[x]);

                                        }
                                    }
                                }

                                function addGroup(userlist)
                                {
                                    var text = document.getElementById("to").value;
                                    var typetext = userlist.contains(text);
                                    if (typetext)
                                    {
                                        var elems = document.getElementsByClassName("list");
                                        for (x in elems)
                                        {
                                            if (elems[x].id == typetext)
                                            {
                                                return;
                                            }
                                        }
                                        var todiv = document.getElementById("email_groups");
                                        todiv.innerHTML += "<p class='list' onclick='removeGroup(\"" + typetext + "\")' id='" + typetext + "'>" + text + "</p>";
                                        if (typetext.indexOf('.') == 0)
                                            document.getElementById(typetext).style.color = "green";
                                        if (typetext.indexOf('*') == 0)
                                            document.getElementById(typetext).style.color = "red";
                                        if (typetext.indexOf('@') == 0)
                                            document.getElementById(typetext).style.color = "white";
                                        console.log(typetext);
                                        document.getElementById("to").value = "";
                                    }
                                }

                                window.onload = function ()
                                {
                                    var userlist = new UserSuggestions();
                                    var oTextbox = new AutoSuggestControl(document.getElementById("to"), userlist);
                                    document.getElementById('to').onkeydown = function (event)
                                    {
                                        if (event.keyCode == 13)
                                        {
                                            addGroup(userlist);
                                        }
                                    }
                                }
                            </script>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p>Subject:</p>
                        </td>
                        <td>
                            <input type="text" name="subject" id="subject"></input>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p>Message:</p>
                        </td>
                        <td>
                            <div id="emailmsg">    
                                <textarea name="body">Something</textarea>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <input onclick="toggleField()" type="button" value="Add Text Message" id="txtmsgtoggle"></input><br>
                            <div id="txtmsg">
                                
                            </div>
                            <script>
                                function toggleField()
                                {
                                    var elem = document.getElementById("txtmsgtoggle");
                                    if (elem.value == "Add Text Message")
                                    {
                                        elem.value = "Remove Text Message";

                                        document.getElementById("txtmsg").innerHTML = "<textarea cols='40' rows='4' name='sms' id='sms' onkeyup='updatekeycount()'></textarea>" +
                                            '<i id="charcount">160/160 characters remaining.</i>';
                                    }
                                    else
                                    {
                                        elem.value = "Add Text Message";
                                        document.getElementById("txtmsg").innerHTML = '';
                                    }
                                }



                                function updatekeycount()
                                {
                                    var count = document.getElementById("charcount");
                                    var sms = document.getElementById("sms");
                                    count.innerHTML = (160 - sms.value.length) + "/160 chars remaining.";
                                    if (sms.value.length > 160)
                                    {
                                        count.innerHTML = "Too long!";
                                    }
                                }
                                var called = false;
                                function isreadytosubmit()
                                {
                                    if (called)
                                        return true;
                                    called = true;
                                    var sendto = document.getElementsByClassName("list");
                                    var form = document.getElementById("sendemail");
                                    for (x = 0; x < sendto.length;x++ )
                                    {
                                        var input = document.createElement("input");

                                        input.setAttribute("type", "hidden");

                                        input.setAttribute("name", "to[]");

                                        input.setAttribute("value", sendto[x].id);

                                        form.appendChild(input);

                                    }
                                    return true;
                                }
                            </script>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <input type="submit" value="Send">
                        </td>
                    </tr>
                </table>
            </form>
        </div>
        
    </body>
</html>