<?php
    
    class userdata {
        public $mysqli;
        public $name,$email,$phone,$carrier,$position,$password;
        public $logged;
        

        function __construct(){
            
            $this->logged = FALSE;
            if(isset($_COOKIE['login']))
            {
                $dbserver = 'localhost';
                $dbusername = 'root';

                $myfile = fopen("/sqlpass.txt", "r") or die("Unable to open file!");
                $dbpassword = fread($myfile,filesize("/sqlpass.txt"));
                fclose($myfile);

                
                $dbpassword = trim($dbpassword);

                $dbname = '4139';
                $this->mysqli = new mysqli($dbserver,$dbusername,$dbpassword,$dbname);
                if ($this->mysqli->connect_error)
                {
                    die("MySQL Server is having issues: " . $this->mysqli->connect_error);
                    echo "Service unavailable.";
                }

                //Email and session hash should be stored in this cookie like this "email:password"
                $eandp = $_COOKIE['login'];

                $email = explode(':',$eandp)[0];
                $session = explode(':',$eandp)[1];

                $result = $this->mysqli->query("SELECT * FROM members WHERE email='".$email."' AND password='".$session."';");
                if($data = $result->fetch_assoc())
                {
                    $this->logged = TRUE;
                    $this->name = $data['name'];
                    $this->position = $data['position'];
                    $this->email = $data['email'];
                    $this->phone = $data['phone'];
                    $this->carrier = $data['carrier'];
                    $this->password = $data['password'];
                }
                $result->close();
            }

        }

        function initmysql(){
            $dbserver = 'localhost';
            $dbusername = 'root';

            $myfile = fopen("/sqlpass.txt", "r") or die("Unable to open file!");
            $dbpassword = fread($myfile,filesize("/sqlpass.txt"));
            fclose($myfile);
            
            $dbpassword = trim($dbpassword);

            $dbname = '4139';
            $this->mysqli = new mysqli($dbserver,$dbusername,$dbpassword,$dbname);
            if ($this->mysqli->connect_error)
            {
                die("MySQL Server is having issues: " . $this->mysqli->connect_error);
                echo "Service unavailable.";
            }
        }

        function check(){
            if(!$this->mysqli)
                $this->initmysql();
            if(!$this->logged)
                die("Not logged in.");
        }

        //Credentials are stored like this: "email:password"
        function login($credentials){
            if(!$this->mysqli)
                $this->initmysql();

            $email = explode(':',$credentials)[0];
            $password = explode(':',$credentials)[1];

	        $password = hash("sha512",$password);
		
            $result = $this->mysqli->query("SELECT * FROM members WHERE email='".$email."' AND password='".$password."';");
            if($data = $result->fetch_assoc())
            {
                $this->name = $data['name'];
                $this->position = $data['position'];
                $this->email = $data['email'];
                $this->phone = $data['phone'];
                $this->carrier = $data['carrier'];
                $this->password = $data['password'];
                return TRUE;
            }
            
            return FALSE;
        }

        function search($query){
            $this->check();

            if($query == ""){
                $result = $this->mysqli->query(
                    "SELECT * FROM members");
            }
            else{
                $result = $this->mysqli->query(
                    "SELECT * FROM members WHERE name LIKE '%".$query."%'
                    OR email LIKE '%".$query."%'
                    OR position LIKE '%".$query."%'
                    OR phone LIKE '%".$query."%'  
                    OR groups LIKE '%".$query."%'              
                    ;");
            }
            $ret;
            while($data = $result->fetch_assoc())
            {
                $ret[] = $data;
            }

            return $ret;
            
        }

        function get_groups(){
            
	    if(!$this->mysqli)
                $this->initmysql();

            $result;
            {
                $result = $this->mysqli->query(
                    "SELECT * FROM groups;");
            }
            $ret;
            while($vals = $result->fetch_assoc())
            {
                $ret[]=$vals;
            }
            return $ret;
        }

        function search_by_exact_name($query){
            $this->check();

            $result;
            if(strpos($query,"@")===0)
            {
                $result = $this->mysqli->query(
                    "SELECT * FROM members WHERE name='".substr($query,1)."';");
            }
            if(strpos($query,".")===0)
            {
                $resgroupcode = $this->mysqli->query(
                    "SELECT code FROM groups WHERE name='".substr($query,1)."';");
                $code = $resgroupcode->fetch_assoc()['code'];
                $resgroupcode->close();
                $result = $this->mysqli->query(
                    "SELECT * FROM members WHERE groups LIKE '%".$code."%';");
                
            }
            if(strpos($query,"*")===0)
            {
                $result = $this->mysqli->query(
                    "SELECT * FROM members WHERE name='".$query."';");
            }
            

            $ret;
            while($vals = $result->fetch_assoc())
            {
                $ret[]=$vals;
            }
            return $ret;
            
        }

        function register($name,$position,$email,$password,$phone,$carrier,$groups){
            if(!$this->mysqli)
                $this->initmysql();
	    
	        $password = hash("sha512",$password);
            if($this->mysqli->query("INSERT INTO members (name,position,email,password,phone,carrier,groups) 
                VALUES ('".$name."','".$position."','".$email."','".$password."','".$phone."','".$carrier."','".implode(' ',$groups)."');"))
            {
                return TRUE;
            }
            return FALSE;
        }
    }
?>
